/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectiondata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pc
 */
public class getDatabase {
    static int i =7;
    private static final String URL ="jdbc:jtds:sqlserver://localhost:5000;databaseName=STUDENT";
    private static final String USER ="Server";
    private static final String PASSWORD = "13112001";
    static Connection con;
    public getDatabase(){
        con = SetConnection.setConnection(URL, USER, PASSWORD);
    }
    public ArrayList<Object> getData(){  
        ArrayList<Object> data = new ArrayList<>();
        try {
            Statement st = con.createStatement();            
            ResultSet result = st.executeQuery("Select *FROM student");
            //ResultSetMetaData meta = result.getMetaData();
            //System.out.println(meta.getColumnTypeName(2)+" "+meta.getColumnCount());
           //System.out.println(meta.getColumnLabel(2)+" "+meta.getColumnName(1));
            while(result.next()){
                int id = result.getInt(1);
                String name = result.getString(2);
                String address = result.getString(3);
                data.add(id);
                data.add(name);
                data.add(address);
                
                //System.out.println(id+"  "+name+"  "+address);               
            }
        } catch (SQLException ex) {
            Logger.getLogger(getDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }finally{try {
            con.close();
            } catch (SQLException ex) {
                Logger.getLogger(getDatabase.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
        return data;
}
    
    public void queryInsert(int id,String name,String address){
        try {
            PreparedStatement pst = con.prepareStatement("Insert into student values(?,?,?)");            
            pst.setInt(1, id);
            pst.setString(2,name);
            pst.setString(3,address);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
